# Copyright 2009, 2011 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm; then
    require github [ user=deluge-torrent tag=${PNV} ]
else
    require pypi # won't build from github autogenerated tarball
fi
require setup-py [ import=setuptools blacklist=2 multibuild=false test=pytest ]
require option-renames [ renames=[ "gtk gui" ] ]
require gtk-icon-cache
require freedesktop-desktop

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="BitTorrent client with a client/server model"
HOMEPAGE="https://deluge.readthedocs.io/"

SLOT="0"
LICENCES="GPL-2"
MYOPTIONS="
    gui          [[ description = [ Install GTK+ based client for the deluge daemon ] ]]
    libnotify    [[ requires = gui ]]
"

# GUI and webinterface are automagic
# TODO libnotify? ( dev-python/pygame[python_abis:*(-)?] [[ note = [ Play sounds ] ]] )
DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
    build+run:
        dev-python/chardet[python_abis:*(-)?]
        dev-python/Mako[python_abis:*(-)?]
        dev-python/Pillow[python_abis:*(-)?]
        dev-python/pyasn1[python_abis:*(-)?]
        dev-python/pyopenssl[python_abis:*(-)?]
        dev-python/pyxdg[python_abis:*(-)?]
        dev-python/rencode[python_abis:*(-)?]
        dev-python/setproctitle[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        dev-python/zopeinterface[python_abis:*(-)?]
        net-p2p/libtorrent-rasterbar[>=1.1.2][python][python_abis:*(-)?]
        net-twisted/Twisted[>=17.1][python_abis:*(-)?]
    run:
        gui? (
            dev-python/pycairo[python_abis:*(-)?]
            gnome-bindings/pygobject:3[python_abis:*(-)?]
            gnome-desktop/librsvg:2
            x11-apps/xdg-utils
            x11-libs/gdk-pixbuf:2.0[gobject-introspection]
            x11-libs/gtk+:3[>=3.10][gobject-introspection]
        )
        libnotify? (
            x11-libs/libnotify[gobject-introspection]
        )
    test:
        dev-python/pytest-twisted[python_abis:*(-)?]
"

# Requires unpackaged pytest-twisted
RESTRICT="test"

UPSTREAM_DOCUMENTATION="
    ${HOMEPAGE}/en/latest/how-to/index.html
    http://dev.deluge-torrent.org/wiki/UserGuide
"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/en/latest/releases/index.html"

deluge_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

deluge_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

